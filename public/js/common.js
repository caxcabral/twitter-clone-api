const postsApiRoute = '/api/posts';

$('#postTextarea').keyup(event => {
    var textbox = $(event.target);
    setButtonStatus();    
});


$("#submitPostButton").click(event => {
  const textbox = $("#postTextarea");
  var data = { content: textbox.val() };

  $.post(postsApiRoute, data, (postData, status, xhr) => {
    var html = createPostHtml(postData);
    $('.postContainer').prepend(html);
    textbox.val('');
    setButtonStatus();
  })
});

$(document).on('click', '.retweetButton', event => {
  let button = $(event.target);
  var postId = getPostElementId(button);
  if(postId === undefined) return console.error('PostId Undefined');

  $.ajax({
    url: `${postsApiRoute}/${postId}/retweet`,
    type: 'POST',
    success: postData => {
      console.log(postData);
    }
  })
});


$(document).on('click', '.likeButton', event => {
  let button = $(event.target);
  var postId = getPostElementId(button);
  if(postId === undefined) return console.error('PostId Undefined');

  $.ajax({
    url: `${postsApiRoute}/${postId}/like`,
    type: 'PUT',
    success: postData => {
      button.find('span').text(postData.likes.length || '');
      const i = button.find('i');

      button.toggleClass('red active', postData.isLiked);
      i.toggleClass('fas', postData.isLiked);
      i.toggleClass('far', !postData.isLiked);
    }
  })
});

const getPostElementId = element => {
  var isRoot = element.hasClass('post');
  var rootElement = isRoot ? element : element.closest('.post');
  var postId = rootElement.data().id;
  return postId;
}

function setButtonStatus() {
  var value = $("#postTextarea").val().trim();
  var submitButton = $("#submitPostButton");

  if (submitButton.length == 0) return console.error("No submit button found");

  if (value == "") return submitButton.prop("disabled", true);
  return submitButton.prop("disabled", false);
}

const units = {
  year: 24 * 60 * 60 * 1000 * 365,
  month: (24 * 60 * 60 * 1000 * 365) / 12,
  day: 24 * 60 * 60 * 1000,
  hour: 60 * 60 * 1000,
  minute: 60 * 1000,
  second: 1000,
};

const rtf = new Intl.RelativeTimeFormat("en", { numeric: "auto" });

const getTimeAgo = (timestamp) => {
  var date = new Date(timestamp);
  var elapsed = date - new Date();
  for (var u in units)
    if (Math.abs(elapsed) > units[u] || u == "second")
      return rtf.format(Math.round(elapsed / units[u]), u);
}


function createPostHtml(postData) {
  const postedBy = postData.postedBy;
  try{
    if (postedBy._id === undefined) return console.warning("User object not populated");
    var displayName = postedBy.firstName + ' ' + postedBy.lastName;
    var timestamp = postData.createdAt;
  } catch {
    console.debug(postedBy);
  }
  
  postData.likes ??= [];

  return `<div class='post' data-id="${postData._id}">
            <div class='mainContentContainer'>
              <div class='userImageContainer'>
                <img src='${postedBy.profilePic}' />
              </div>
              <div class='postContentContainer'>
                <div class='header'>
                  <a href='/profile/${postedBy.username}' 
                    class='displayName'>${displayName}</a>
                  <span class='username'>@${postedBy.username}</span>
                  <span class='date'>${getTimeAgo(timestamp)}</span>
                </div>

                <div class='postBody'>
                  ${postData.content}
                </div>

                <div class='postFooter'>

                  <div class='postButtonContainer'>
                    <button>
                      <i class="far fa-comment"></i>
                    </button>
                  </div>
                  
                  <div class='postButtonContainer'>
                    <button class="retweetButton">
                      <i class="fas fa-retweet"></i>
                    </button>
                  </div>

                  <div class='postButtonContainer '>
                    <button class="likeButton ${postData.isLiked ? "red active" : ""}">
                      <i class="${postData.isLiked ? "fas" : "far"} fa-heart"></i>
                      <span>${postData.likes.length || ""}</span>
                    </button>  
                  </div>

                </div>
              </div>
            </div>
          </div>`;
}

