import mongoose from 'mongoose';

const { DB_USER, DB_PASS, DB_HOST, DB_NAME } = process.env;


const url = `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_NAME}?retryWrites=true&w=majority`;

mongoose.connect(url);

mongoose.connection.on('error', () => console.error('Connection error:'));
mongoose.connection.once('open', () => console.log('Database connected.'));