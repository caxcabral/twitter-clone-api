import express from "express";

const app = express();
app.use(express.urlencoded({ extended: true }));

const logoutRouter = express.Router();

logoutRouter.get("/", (req, res, next) => {
    if(req.session) req.session.destroy();
    res.redirect('/login');
});

export { logoutRouter }
