import express from 'express';
import multer from 'multer';
import bcrypt from 'bcrypt';
import User from '../schemas/userSchema.js';

const app = express();
app.use(express.urlencoded({ extended: true }));

app.set("view engine", "pug");
app.set("views", "views");

const loginRouter = express.Router();

loginRouter.get('/', (req, res, next) => {
    if(req.session.user) return res.redirect('/');

    var payload = {
        pageTitle: 'Login',
    }

    res.status(200).render('login', payload);
});

loginRouter.post('/', multer().none(), async (req, res, next) => {
    
    var payload = req.body;
    
    if(req.body.logUsername && req.body.logPassword) {
        var user = await User.findOne({
            $or: [
                {username: payload.logUsername},
                {email: payload.logEmail}
            ]
        }).catch((error) => {
            console.log(error);
        })

        if(user != null) {
            var result = await bcrypt.compare(req.body.logPassword, user.password);

            if(result === true) {
                req.session.user = user;
                return res.redirect('/');
            } else {
                payload.errorMessage = 'Credentials Incorrect';
            }
        } else {
            payload.errorMessage = 'Email not registered'
        }
    } else {
        payload.errorMessage = 'Make sure each field has a valid value.';
    }
    return res.status(200).render('login', payload);
})

export { loginRouter };

