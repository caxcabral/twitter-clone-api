import express from 'express';
import multer from 'multer';
import User from '../schemas/userSchema.js';
import bcrypt from 'bcrypt';



const app = express();
app.set('view engine', 'pug');
app.set('views', 'views');

app.use(express.urlencoded({ extended: true }));

const registerRouter = express.Router();

registerRouter.get('/', (req, res, next) => {
    res.status(200).render('register');
});

registerRouter.post('/', multer().none(), async (req, res, next) => {

    var firstName = req.body.firstName.trim();
    var lastName = req.body.lastName.trim();
    var username = req.body.username.trim();
    var email = req.body.email.trim();
    var password = await bcrypt.hash(req.body.password, 10);

    
    var payload = {firstName, lastName, username, email, password};
    
    if (firstName && lastName && username && email && password) {
        const user = await User.findOne({
            $or: [
                { username: username }, 
                { email: email }
            ],
        }).catch((error) => {
            console.error(error);
            payload.errorMessage = "Something went wrong.";
        });

        if (user == null) {
            //no user was found
            const newUser = await User.create({
                firstName,
                lastName,
                username,
                email,
                password,
            }).catch((error) => {
                console.error('Erro ao criar usuario:', error);
            });
            console.debug(req);
            req.session.user = newUser;
            return res.redirect('/');

        } else {
            //user found
            if (email == user.email) {
                payload.errorMessage = "Email already in use.";
            } else {
                payload.errorMessage = "Username already in use";
            }
        }

    } else {
        payload.errorMessage = "Make sure each field has a valid value.";
    }
    res.status(200).render("register", payload);
});

export { registerRouter };

