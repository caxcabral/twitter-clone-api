import express from 'express';
import multer from 'multer';
import { requireLogin } from '../../middleware.js';
import Post from '../../schemas/postSchema.js';
import User from '../../schemas/userSchema.js';


const apiPostsRouter = express.Router();
apiPostsRouter.use(express.urlencoded({ extended: true }));

apiPostsRouter.get("/", async (req, res) => {    
    let posts = await Post
        .find()
        .populate('postedBy')
        .sort({'createdAt': -1})
        .lean()
        .catch(error => { 
            console.error(error);
            return res.sendStatus(400);
        })

    const currentUser = req.session.user;
    if (currentUser) {
        posts.forEach(post => {
            post.isLiked = currentUser.likes.some(like => like == post._id);
        });	
    }
    return res.status(200).send(posts);
});

apiPostsRouter.post('/', 
	requireLogin,
	
	multer().none(), 
	
	async (req, res, next) => {
		if (!req.body.content) {
			console.log('Content param not sent with request');
			res.sendStatus(400);
		}

		const postData = {
			content: req.body.content,
			postedBy: req.session.user
		}

		var newPost = await Post
			.create(postData)
			.catch(error => {
				console.error(error);
				return res.statusStatus(400);
			});

		newPost = await User
			.populate(newPost, { path: 'postedBy' });

		return res.status(201).send(newPost);
	}
)

apiPostsRouter.post('/:id/retweet',
	requireLogin,

	multer().none(), 

	async (req, res, next) => {
		const postId = req.params.id;
		const currentUser = req.session.user;
		
		const userId = currentUser._id;
		return res.status(200).send('retweeted');
	}
)

apiPostsRouter.get('/:id/like', async (req, res, next) => {
    try {
        const postId = req.params.id;
        const post = await Post.findById(postId);

        if (!post) return res.sendStatus(404);
        return res.status(200).send(post.likes);

    } catch(error) { 
        console.error(error);
        return res.status(503);
    }
})


apiPostsRouter.delete("/:id", 
	requireLogin,

	async (req, res, next) => {
		const currentUser = req.session.user._id;
		const postId = req.params.id;
		const post = await Post.findById(postId)
			.populate("postedBy")
			.catch((error) => {
				console.error(error);
				return res.status(404).send("Something went wrong.");
			});

		const postOwner = post.postedBy._id;

		// if authenticated user is not post owner return unauthorized.
		if (currentUser != postOwner) return res.sendStatus(401);

		await Post.findByIdAndDelete(postId);
	}
); 

apiPostsRouter.put('/:id/like', 
	requireLogin,

	async (req, res, next) => {
		const postId = req.params.id;
		const userId = req.session.user._id;

		var isLiked = 
			req.session.user.likes && 
			req.session.user.likes.includes(postId);

		var option = isLiked
			? '$pull'
			: '$addToSet'
		
		try {
			//update user
			var newUser = await User.findByIdAndUpdate(
				userId,
				{ [option]: { likes: postId } },
				{ new: true }
			);
			req.session.user = newUser;

			//update post object
			var newPost = await Post.findByIdAndUpdate(
				postId,
				{ [option]: { likes: userId } },
				{ new: true }
			).lean();
			
			newPost.likedByUser = isLiked;

			return res.status(200).send(newPost);
		} catch(error) {
			console.error(error);
			return res.sendStatus(400);
		}
	}
)

export { apiPostsRouter };