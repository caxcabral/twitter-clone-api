import './setup/db.js';
import express from 'express';
import session from 'express-session';

import { requireLogin } from './middleware.js';


const sessionData = {
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true
};

const app = express();

app.set('view engine', 'pug');
app.set('views', 'views');

app.use(session(sessionData));
app.use(express.static('public'));

import { loginRouter } from './routes/loginRoutes.js';
import { registerRouter } from './routes/registerRoutes.js';
import { logoutRouter } from './routes/logoutRoutes.js';
import { apiPostsRouter } from './routes/api/postRoutes.js';

app.use('/login', loginRouter);
app.use('/signout', logoutRouter);
app.use('/register', registerRouter);
app.use('/api/posts', apiPostsRouter);

app.get('/', requireLogin,  (req, res, next) => {
    var payload = {
        user: req.session.user,
        pageTitle: 'Home'
    }
    return res.status(200).render('home', payload);
});


const PORT = process.env.PORT ?? 3000;
app.listen(PORT, () => console.log(`Listening on port ${PORT}.`)); 